<?php
class PostTags extends EntidadBase{
    private $pt_id;
    private $pt_name;
    private $pt_hastag;

    public function __construct($adapter){
        $table = "post_tags";
        parent:: __construct($table, $adapter);
    }

    public function getpt_id()
    {
        return $this->pt_id;
    }
    public function setpt_id($pt_id)
    {
        $this->pt_id = $pt_id;
    }

    public function getpt_name()
    {
        return $this->pt_name;
    }
    public function setpt_name($pt_name)
    {
        $this->pt_name = $pt_name;
    }

    public function getpt_hastag()
    {
        return $this->pt_hastag;
    }
    public function setpt_hastag($pt_hastag)
    {
        $this->pt_hastag = $pt_hastag;
    }

    
}
?>