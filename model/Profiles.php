<?php 
class Profiles extends EntidadBase{
    private $in_id;
    private $in_name;
    private $in_biography;
    private $in_sign;
    private $in_profile_photo;
    private $in_profile;

    public function __construct($adapter)
    {
        $table = "info_people";
        parent:: __construct($table, $adapter);
    }

    public function getin_id()
    {
        return $this->in_id;
    }
    public function setin_id($in_id)
    {
        $this->in_id = $in_id;
    }

    public function getin_name()
    {
        return $this->in_name;
    }
    public function setin_name($in_name)
    {
        $this->in_name = $in_name;
    }

    public function getin_biography()
    {
        return $this->in_biography;
    }
    public function setin_biography($in_biography)
    {
        $this->in_biography = $in_biography;
    }

    public function getin_sign()
    {
        return $this->in_sign;
    }
    public function setin_sign($in_sign)
    {
        $this->in_sign = $in_sign;
    }

    public function getin_profile_photo()
    {
        return $this->in_profile_photo;
    }
    public function setin_profile_photo($in_profile_photo)
    {
        $this->in_profile_photo = $in_profile_photo;
    }

    public function getin_profile()
    {
        return $this->in_profile;
    }
    public function setin_profile($in_profile)
    {
        $this->in_profile = $in_profile;
    }


    public function getLawyer($in_type)
    {
        $query = $this->db()->query("SELECT * FROM info_people WHERE in_type = $in_type");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
    }



}

?>