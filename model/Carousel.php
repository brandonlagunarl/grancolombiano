<?php
class Carousel extends EntidadBase{
    private $cr_id;
    private $cr_title;
    private $cr_head_title;
    private $cr_text_content;
    private $cr_url_redirect;
    private $cr_main_image;

    public function __construct($adapter){
        $table = "carousel_attorna";
        parent:: __construct($table, $adapter);
    }

    public function getcr_id()
    {
        return $this->cr_id;
    }
    public function setcr_id($cr_id)
    {
        $this->cr_id = $cr_id;
    }

    public function getcr_title()
    {
        return $this->cr_title;
    }
    public function setcr_title($cr_title)
    {
        $this->cr_title = $cr_title;
    }

    public function getcr_head_title()
    {
        return $this->cr_head_title;
    }
    public function setcr_head_title($cr_head_title)
    {
        $this->cr_head_title = $cr_head_title;
    }

    public function getcr_text_content()
    {
        return $this->cr_text_content;
    }
    public function setcr_text_content($cr_text_content)
    {
        $this->cr_text_content = $cr_text_content;
    }

    public function getcr_url_redirect()
    {
        return $this->cr_url_redirect;
    }
    public function setcr_url_redirect($cr_url_redirect)
    {
        $this->cr_url_redirect = $cr_url_redirect;
    }

    public function getcr_main_image()
    {
        return $this->cr_main_image;
    }
    public function setcr_main_image($cr_main_image)
    {
        $this->cr_main_image = $cr_main_image;
    }



    public function getCarousel()
    {
        $query = $this->db()->query("SELECT * FROM carousel_attorna WHERE activate = '1'");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
    }
    
}
?>