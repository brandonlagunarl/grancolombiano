<?php
class ImageBlog extends EntidadBase{
    private $ib_id;
    private $ib_url;
    private $ib_type_image;
    private $ib_bgp_id;
    private $ib_upload;

    public function __construct($adapter){
        $table = "image_blog";
        parent:: __construct($table, $adapter);
    }

    public function getib_id()
    {
        return $this->ib_id;
    }
    public function setib_id($ib_id)
    {
        $this->ib_id = $ib_id;
    }

    public function getib_url()
    {
        return $this->ib_url;
    }
    public function setib_url($ib_url)
    {
        $this->ib_url = $ib_url;
    }

    public function getib_type_image()
    {
        return $this->ib_type_image;
    }
    public function setib_type_image($ib_type_image)
    {
        $this->ib_type_image = $ib_type_image;
    }

    public function getib_bgp_id()
    {
        return $this->ib_bgp_id;
    }
    public function setib_bgp_id($ib_bgp_id)
    {
        $this->ib_bgp_id = $ib_bgp_id;
    }

    public function getImages($id)
    {
        $query = $this->db()->query("SELECT * FROM image_blog WHERE ib_bgp_id = $id");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
        }else{
            $resultSet = [];
        }
        return $resultSet;
    }

}
?>