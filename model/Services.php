<?php
class Services extends EntidadBase{
    private $bs_id;
    private $bs_title;
    private $bs_content;
    private $bs_icon;
    private $bs_image;

    public function __construct($adapter)
    {
        $table = "business_services";
        parent:: __construct($table, $adapter);
    }

    public function getbs_id()
    {
        return $this->bs_id;
    }
    public function setbs_id($bs_id)
    {
        $this->bs_id = $bs_id;
    }

    public function getbs_title()
    {
        return $this->bs_title;
    }
    public function setbs_title($bs_title)
    {
        $this->bs_title = $bs_title;
    }

    public function getbs_content()
    {
        return $this->bs_content;
    }
    public function setbs_content($bs_content)
    {
        $this->bs_content = $bs_content;
    }

    public function getbs_icon()
    {
        return $this->bs_icon;
    }
    public function setbs_icon($bs_icon)
    {
        $this->bs_icon = $bs_icon;
    }

    public function getbs_image()
    {
        return $this->bs_image;
    }
    public function setbs_image($bs_image)
    {
        $this->bs_image = $bs_image;
    }

    public function getServices($limit)
    {
        $query = $this->db()->query("SELECT * FROM business_services WHERE bs_active ='1' LIMIT $limit");
        if($query->num_rows > 0){
            while ($row = $query->fetch_object()) {
               $resultSet[]=$row;
            }
         
         return $resultSet;
        }
    }

}


?>