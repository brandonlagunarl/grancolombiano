<?php
class PortafolioController extends ControladorBase{
    private $adapter;
    private $conectar;

    public function __construct() {
       parent::__construct();

       $this->conectar=new Conectar();
        $this->adapter=$this->conectar->conexion();
    }

    public function Index() {

        $this->view("portafolio/index",array());
    }
}

?>