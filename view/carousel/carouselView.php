
<div class=attorna-page-wrapper id=attorna-page-wrapper>
            <div class=gdlr-core-page-builder-body>
                <div class="gdlr-core-pbf-wrapper " style="padding: 0px 0px 0px 0px;">
                    <div class=gdlr-core-pbf-background-wrap></div>
                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-pbf-wrapper-full-no-space">
                            <div class=gdlr-core-pbf-element>
                                <div class="gdlr-core-revolution-slider-item gdlr-core-item-pdlr gdlr-core-item-pdb " style="padding-bottom: 0px ;">
                                    <div id=rev_slider_1_1_wrapper class="rev_slider_wrapper fullwidthbanner-container" data-source=gallery style="margin:0px auto;background:#202020;padding:0px;margin-top:0px;margin-bottom:0px;">
                                        <div id=rev_slider_1_1 class="rev_slider fullwidthabanner" style=display:none; data-version=5.4.8.2>
                                            <ul>
                                            <?php
                                                foreach($carousel as $frame)
                                                {
                                            ?>          
                                                <li data-index="rs<?=$frame->cr_id?>" data-transition=fade data-slotamount=default data-hideafterloop=0 data-hideslideonmobile=off data-easein=default data-easeout=default data-masterspeed=300 data-thumb="<?=$frame->cr_main_image?>" data-rotate=0 data-saveperformance=off data-title=Slide data-param1 data-param2 data-param3 data-param4 data-param5 data-param6 data-param7 data-param8 data-param9 data-param10 data-description> <img src="<?=$frame->cr_main_image?>" alt title=slider-1 width=1800 height=1000 data-bgposition="center center" data-bgfit=cover data-bgrepeat=no-repeat class=rev-slidebg data-no-retina>
                                                    <div class="tp-caption   tp-resizeme" id="slide-<?=$frame->cr_id?>-layer-1" data-x=center data-hoffset data-y=center data-voffset=-182 data-width="['auto']" data-height="['auto']" data-type=text data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop=[0,0,0,0] data-paddingright=[0,0,0,0] data-paddingbottom=[0,0,0,0] data-paddingleft="[0,0,0,0]" style="z-index: 5; white-space: nowrap; font-size: 38px; line-height: 38px; font-weight: 400; color: #b6a281; letter-spacing: 2px;font-family:Poppins;text-transform:uppercase;"><?=$frame->cr_title?></div>
                                                    <div class="tp-caption tp-shape tp-shapewrapper  tp-resizeme" id="slide-<?=$frame->cr_id?>-layer-3" data-x=center data-hoffset data-y=center data-voffset=-152 data-width="['103']" data-height="['1']" data-type=shape data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop=[0,0,0,0] data-paddingright=[0,0,0,0] data-paddingbottom=[0,0,0,0] data-paddingleft="[0,0,0,0]" style="z-index: 6;background-color:rgb(182,162,129);"></div>
                                                    <div class="tp-caption   tp-resizeme" id="slide-<?=$frame->cr_id?>-layer-4" data-x=center data-hoffset data-y=center data-voffset=-80 data-width="['auto']" data-height="['auto']" data-type=text data-responsive_offset="on" data-frames='[{"delay":310,"speed":300,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop=[0,0,0,0] data-paddingright=[0,0,0,0] data-paddingbottom=[0,0,0,0] data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 102px; line-height: 102px; font-weight: 700; color: #ffffff; letter-spacing: 8px;font-family:Poppins;text-transform:uppercase;"><?=$frame->cr_head_title?></div>
                                                    <div class="tp-caption   tp-resizeme" id="slide-<?=$frame->cr_id?>-layer-5" data-x=center data-hoffset data-y=center data-voffset=33 data-width="['610']" data-height="['81']" data-visibility="['on','on','off','off']" data-type=text data-responsive_offset="on" data-frames='[{"delay":530,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]' data-textAlign="['center','center','center','center']" data-paddingtop=[0,0,0,0] data-paddingright=[0,0,0,0] data-paddingbottom=[0,0,0,0] data-paddingleft="[0,0,0,0]" style="z-index: 8; min-width: 610px; max-width: 610px; max-width: 81px; max-width: 81px; white-space: normal; font-size: 20px; line-height: 38px; font-weight: 500; color: #dedede; letter-spacing: 0px;font-family:Poppins;"><?=$frame->cr_text_content?></div>
                                                    <a href="<?=$frame->cr_url_redirect?>"><div class="tp-caption rev-btn rev-withicon " id="slide-<?=$frame->cr_id?>-layer-7" data-x=center data-hoffset data-y=center data-voffset=127 data-width="['auto']" data-height="['auto']" data-visibility="['on','on','off','off']" data-type=button data-responsive_offset="on" data-frames='[{"delay":790,"speed":300,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(174,148,104);bs:solid;bw:0 0 0 0;"}]' data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop=[14,14,14,14] data-paddingright=[17,17,17,17] data-paddingbottom=[16,16,16,16] data-paddingleft="[23,23,23,23]" style="z-index: 9; white-space: nowrap; font-size: 12px; line-height: 17px; font-weight: 700; color: rgba(255,255,255,1); letter-spacing: 2.5px;font-family:Poppins;text-transform:uppercase;background:linear-gradient(180deg, rgba(131,102,62,1) 0%, rgba(176,150,106,1) 100%);border-color:rgba(0,0,0,1);border-radius:3px 3px 3px 3px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Conocer mas<i class=fa-icon-long-arrow-right></i></div></a>
                                                </li>

                                            <?php
                                                }
                                            ?>
                                            </ul>
                                            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
