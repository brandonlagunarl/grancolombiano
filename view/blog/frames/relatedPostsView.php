<div class="attorna-single-related-post-wrap attorna-item-rvpdlr">
        <h3 class="attorna-single-related-post-title attorna-item-mglr">Articulos Relacionados</h3>
                                <div class="gdlr-core-blog-item-holder clearfix">
                                    <?php foreach($related as $article){?>
                                    <div class="gdlr-core-item-list  gdlr-core-item-pdlr gdlr-core-column-30 ">
                                        <div class="gdlr-core-blog-grid gdlr-core-style-3  gdlr-core-with-thumbnail">
                                            <div class="gdlr-core-blog-thumbnail gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
                                                <a href="blog?action=read&s=<?=$article->bgp_id?>">
                                                <?php
                                                $frameviews = "blog/frame".$article->bgp_cover_type;
                                                $this->frameview("$frameviews",array("cover"=>$article->bgp_url_cover,"w"=>"700","h"=>"450"));
                                                ?>
                                                </a>
                                            </div>
                                            <div class=gdlr-core-blog-grid-content-wrap>
                                                <h3 class="gdlr-core-blog-title gdlr-core-skin-title" style="font-size: 16px ;"><a href="blog?action=read&s=<?=$article->bgp_id?>" ><?=$article->bgp_title?></a></h3>
                                                <div class="gdlr-core-blog-info-wrapper gdlr-core-skin-divider"><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"><span class=gdlr-core-blog-info-sep >•</span><span class=gdlr-core-head><i class=icon_clock_alt ></i></span><a href=#><?=$article->bgp_date_modified?></a></span><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-author"><span class=gdlr-core-blog-info-sep >•</span><span class=gdlr-core-head><i class=icon_documents_alt ></i></span><a href=# title="Posts by <?=$article->pa_name?>" rel=author> <?=$article->pa_name?></a></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>