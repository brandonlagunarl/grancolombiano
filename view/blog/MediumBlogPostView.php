<?php foreach($posts as $frame){?>
    <div class="gdlr-core-item-list gdlr-core-blog-full gdlr-core-style-3  gdlr-core-item-mglr gdlr-core-style-left gdlr-core-with-thumbnail">
    <div class="gdlr-core-blog-thumbnail gdlr-core-media-image  gdlr-core-opacity-on-hover gdlr-core-zoom-on-hover">
        <a href=#>
        <?php
            $frameviews = "blog/frame".$frame->bgp_cover_type;
            $this->frameview("$frameviews",array("cover"=>$frame->bgp_url_cover,"w"=>"900","h"=>"500"));
        ?>
        </a>
</div>
<div class="gdlr-core-blog-full-frame gdlr-core-skin-e-background" style="padding-top: 50px;padding-bottom: 50px;">
    <div class="gdlr-core-blog-full-head clearfix">
        <h3 class="gdlr-core-blog-title gdlr-core-skin-title" style="font-size: 27px ;font-weight: 700 ;letter-spacing: 0px ;"><a href=# ><?=$frame->bgp_title?></a></h3></div>
            <div class=gdlr-core-blog-content><?=$frame->bgp_content?></div>
            <div class="gdlr-core-blog-info-wrapper gdlr-core-skin-divider"><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-date"><span class=gdlr-core-blog-info-sep >•</span><span class=gdlr-core-head><i class=icon_clock_alt ></i></span><a href=#> <?=$frame->bgp_date_modified?></a></span><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-author"><span class=gdlr-core-blog-info-sep >•</span><span class=gdlr-core-head><i class=icon_documents_alt ></i></span><a href="Author?name=<?=$frame->pa_name?>" title="Posts by <?=$frame->pa_name?>" rel=author><?=$frame->pa_name?></a></span><span class="gdlr-core-blog-info gdlr-core-blog-info-font gdlr-core-skin-caption gdlr-core-blog-info-tag"><span class=gdlr-core-blog-info-sep >•</span><span class=gdlr-core-head><i class=icon_tags_alt ></i></span><a href=<?=$frame->pt_name?> rel=tag><?=$frame->pt_name?></a>
    </div><a class="gdlr-core-excerpt-read-more gdlr-core-button gdlr-core-rectangle" href="?action=read&s=<?=$frame->bgp_id?>">Read More</a></div>
</div>

<?php } ?>