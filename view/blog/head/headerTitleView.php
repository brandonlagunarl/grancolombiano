<?php foreach($posts as $content){}?>
<div class="attorna-blog-title-wrap  attorna-style-custom attorna-feature-image" style="background-image: url(<?=$content->bgp_url_cover?>) ;">
                <div class=attorna-header-transparent-substitute></div>
                <div class=attorna-blog-title-overlay style="opacity: 0.01 ;"></div>
                <div class=attorna-blog-title-bottom-overlay></div>
                <div class="attorna-blog-title-container attorna-container">
                    <div class="attorna-blog-title-content attorna-item-pdlr" style="padding-top: 300px ;padding-bottom: 20px ;">
                        <header class="attorna-single-article-head clearfix">
                            <div class=attorna-single-article-head-right>
                                <h1 class="attorna-single-article-title"><?=$content->bgp_title?></h1>
                                
                                    <div class=attorna-blog-info-wrapper>
                                        <div class="attorna-blog-info attorna-blog-info-font attorna-blog-info-author vcard author post-author ">
                                            <span class=attorna-blog-info-sep>•</span>
                                            <span class=attorna-head>
                                                <i class=icon_documents_alt ></i>
                                            </span>
                                            <span class=fn>
                                                <a href=# title="Posts by James Smith" rel=author><?=$content->pa_name?></a>
                                            </span>
                                        </div>
                                        <div class="attorna-blog-info attorna-blog-info-font attorna-blog-info-category ">
                                            <span class=attorna-blog-info-sep>•</span>
                                            <span class=attorna-head>
                                                <i class=icon_folder-alt ></i>
                                            </span>
                                            <a href=# rel=tag>Blog</a>
                                            <span class=gdlr-core-sep>,</span>
                                            <a href=# rel=tag>Post Format</a>
                                        </div>
                                        <div class="attorna-blog-info attorna-blog-info-font attorna-blog-info-comment ">
                                            <span class=attorna-blog-info-sep>•</span>
                                            <span class=attorna-head>
                                                <i class=icon_comment_alt ></i>
                                            </span>
                                            <a href=#>no comments</a>
                                        </div>
                                    </div>
                            </div>
                        </header>
                    </div>
                </div>
            </div>