<?php foreach($posts as $article)?>
<article id=post-1268 class="post-1268 post type-post status-publish format-standard has-post-thumbnail hentry category-blog category-post-format">
<div class="attorna-single-article clearfix">
     <div class=attorna-single-article-content>
     <?=$article->bgp_content?>
        <ul class="wp-block-gallery aligncenter columns-5 is-cropped">
        <?php foreach($gallery as $figure){ ?>
            <li class=blocks-gallery-item>
                <figure>
                <a href=<?=$figure->ib_url?>><img class=wp-image-6031 src=<?=$figure->ib_url?> alt></a>
                </figure>
            </li>
        <?php } ?>
        </ul></div>
</div>
</article>