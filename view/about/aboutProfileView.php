<?php foreach($lawyer as $profile) {}?>
<div class="gdlr-core-pbf-wrapper " style="margin: -70px 0px 0px 0px;padding: 0px 0px 0px 0px;" id=gdlr-core-wrapper-1>
                    <div class=gdlr-core-pbf-background-wrap></div>
                    <div class="gdlr-core-pbf-wrapper-content gdlr-core-js ">
                        <div class="gdlr-core-pbf-wrapper-container clearfix gdlr-core-container">
                            <div class=" gdlr-core-pbf-wrapper-container-inner gdlr-core-item-mglr clearfix" style="box-shadow: 0 0 32px rgba(10, 10, 10,0.08); -moz-box-shadow: 0 0 32px rgba(10, 10, 10,0.08); -webkit-box-shadow: 0 0 32px rgba(10, 10, 10,0.08); background-color: #ffffff ;border-radius: 4px 4px 4px 4px;-moz-border-radius: 4px 4px 4px 4px;-webkit-border-radius: 4px 4px 4px 4px;">
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-column-first" id=gdlr-core-column-1>
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " style="padding: 60px 40px 20px 45px;" data-sync-height=height-about>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content">
                                            <div class=gdlr-core-pbf-element>
                                                <div class="gdlr-core-title-item gdlr-core-item-pdb clearfix  gdlr-core-left-align gdlr-core-title-item-caption-top gdlr-core-item-pdlr" style="padding-bottom: 8px ;">
                                                    <div class="gdlr-core-title-item-title-wrap ">
                                                        <h3 class="gdlr-core-title-item-title gdlr-core-skin-title " style="font-size: 16px ;font-weight: 700 ;letter-spacing: 4px ;">Acerca de Juridico Grancolombiano<span class="gdlr-core-title-item-title-divider gdlr-core-skin-divider" ></span></h3></div>
                                                </div>
                                            </div>
                                            <div class=gdlr-core-pbf-element>
                                                <div class="gdlr-core-divider-item gdlr-core-divider-item-normal gdlr-core-item-pdlr gdlr-core-left-align">
                                                    <div class=gdlr-core-divider-container style="max-width: 39px ;">
                                                        <div class="gdlr-core-divider-line gdlr-core-skin-divider" style="border-color: #b1976b ;border-bottom-width: 4px ;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=gdlr-core-pbf-element>
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align" style="padding-bottom: 15px ;">
                                                    <div class=gdlr-core-text-box-item-content style="text-transform: none ;">
                                                        <p><?=$profile->in_biography?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class=gdlr-core-pbf-element>
                                                <div class="gdlr-core-image-item gdlr-core-item-pdlr gdlr-core-item-pdb  gdlr-core-left-align">
                                                    <div class="gdlr-core-image-item-wrap gdlr-core-media-image  gdlr-core-image-item-style-rectangle" style="border-width: 0px;"><img src="<?=$profile->in_sign?>" alt width=254 height=47 title=sign-black></div>
                                                </div>
                                            </div>
                                            <div class=gdlr-core-pbf-element>
                                                <div class="gdlr-core-text-box-item gdlr-core-item-pdlr gdlr-core-item-pdb gdlr-core-left-align">
                                                    <div class=gdlr-core-text-box-item-content style="font-size: 11px ;font-weight: 400 ;letter-spacing: 4px ;text-transform: uppercase ;">
                                                        <p><?=$profile->in_profile?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gdlr-core-pbf-column gdlr-core-column-30 gdlr-core-hide-in-mobile">
                                    <div class="gdlr-core-pbf-column-content-margin gdlr-core-js " data-sync-height=height-about>
                                        <div class=gdlr-core-pbf-background-wrap style="border-radius: 0px 4px 4px 0px;-moz-border-radius: 0px 4px 4px 0px;-webkit-border-radius: 0px 4px 4px 0px;">
                                            <div class="gdlr-core-pbf-background gdlr-core-parallax gdlr-core-js" style="background-image: url(<?=$profile->in_profile_photo?>) ;background-size: cover ;background-position: center ;" data-parallax-speed=0></div>
                                        </div>
                                        <div class="gdlr-core-pbf-column-content clearfix gdlr-core-js  gdlr-core-sync-height-content"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>