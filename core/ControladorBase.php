<?php
class ControladorBase{

    public function __construct() {
		require_once 'Conectar.php';
        require_once 'EntidadBase.php';
        require_once 'ModeloBase.php';
        //Incluir todos los modelos
        foreach(glob("model/*.php") as $file){
            require_once $file;
        }
        
    }
    
    //Plugins y funcionalidades
    
    public function view($vista,$datos){
        require_once 'config/web_config.php';
        require_once 'config/Robot.php';
        foreach ($datos as $id_assoc => $valor) {
            ${$id_assoc}=$valor; 
        }
        require_once '_construct/heading/heading.php';
        require_once 'core/AyudaVistas.php';
        $helper=new AyudaVistas();
        require_once 'view/'.$vista.'View.php';
        
        require_once '_construct/footer/footer.php';

    }
    
    function frameview($vista,$datos)
    {
        foreach ($datos as $id_assoc => $valor) {
            ${$id_assoc}=$valor; 
        }
        require_once 'core/AyudaVistas.php';
            $helper=new AyudaVistas();
            require 'view/'.$vista.'View.php';
    }
    public function redirect($controlador=CONTROLADOR_DEFECTO,$accion=ACCION_DEFECTO){
        if($accion != ''){
        header("Location:/juridicograncolombiano/".$controlador."?action=".$accion);
        }
        else{
            header("Location:/juridicograncolombiano/".$controlador);

        }
    }
            //Métodos para los controladores

    public function email($vista,$datos){
        
        foreach ($datos as $id_assoc => $valor) {
            ${$id_assoc}=$valor; 
            
        }
        require_once 'core/AyudaVistas.php';
        $helper=new AyudaVistas();
        require_once 'mail/'.$vista.'View.php';

    }


}
?>
